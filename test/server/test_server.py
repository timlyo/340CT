import unittest
import requests

class TestRoutes(unittest.TestCase):
    def test_index_response(self):
        r = requests.get("http://127.0.0.1:5000")
        assert(r.status_code == 200)

    def test_html_has_head(self):
        r = requests.get("http://127.0.0.1:5000")
        html = r.text
        assert("<head>" in html)
        assert("</head>" in html)

    def test_html_has_body(self):
        r = requests.get("http://127.0.0.1:5000")
        html = r.text
        assert("<body>" in html)
        assert("</body>" in html)

    def test_html_has_style(self):
        r = requests.get("http://127.0.0.1:5000")
        html = r.text
        assert("<link rel=\"stylesheet\" href=\"/static/css/style.css\">" in html)

if __name__ == "__main__":
    unittest.main()
