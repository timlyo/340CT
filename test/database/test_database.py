import unittest
import requests

# Database url
url = "http://127.0.0.1:8000"

class TestStudent(unittest.TestCase):
    def test_add_get_delete(self):
        r = requests.put(url + "/student", data={"id": 8291, "name": "test", "course": "CS"})
        self.assertEqual(r.status_code, 200)

        r = requests.get(url + "/student", params={"id": 8291})
        data = r.json()

        self.assertEqual(data["id"], 8291)
        self.assertEqual(data["name"], "test")
        self.assertEqual(data["course"], "CS")

        r = requests.delete(url + "/student", params={"id": 8291})
        self.assertEqual(r.status_code, 200)

        r = requests.get(url + "/student", params={"id": 8291})
        self.assertEqual(r.status_code, 404)
        self.assertEqual(r.json(), None)


if __name__ == "__main__":
    unittest.main()
