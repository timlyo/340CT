	git clone git@gitlab.com:timlyo/340CT.git

install the python modules:

	flask passlib requests hug

`sudo pip install flask passlib requests hug` should do this for you

run
	python main.py
	hug -f database/main.py

You'll also need postgresql running with the tables setup. This is dependant on your system. The scripts in database/ can setup the tables and add some dummy data. Good Luck :p

go to `127.0.0.1:5000` in a browser
