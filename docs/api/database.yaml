swagger: "2.0"
info:
  title: Database API
  description: REST API for 340CT project's database
  version: 0.1.0

paths:
  /student/:
    get:
      summary: Get a list of a user’s details
      parameters:
        - name: id
          in: query
          required: true
          type: number
      responses:
        200:
          description: student's details
          schema:
            $ref: '#/definitions/Student'
    post:
      summary: Update a student's details
      parameters:
        - name: id
          in: query
          required: true
          type: number
        - name: name
          in: query
          type: string
      responses:
          200:
            description: student's details
    put:
      summary: Add a new student record
      parameters:
        - name: id
          in: query
          type: number
        - name: name
          in: query
          type: string
      responses:
        201:
          description: Success
    delete:
      summary: Delete a student's record
      parameters:
        - name: id
          in: query
          type: number
      responses:
        200:
          description: OK

  /staff/:
    get:
      summary: Get a staff member's details
      parameters:
        - name: id
          in: query
          type: number
      responses:
        200:
          description: staff details
          schema:
            $ref: '#/definitions/Staff'
    post:
      summary: Update a staff member's details
      parameters:
        - name: id
          in: query
          type: number
          required: true
        - name: name
          type: string
          in: query
      responses:
        200:
          description: OK
    put:
      summary: Add a new staff member record
      parameters:
        - name: id
          type: number
          in: query
          required: true
        - name: name
          type: string
          in: query
          required: true
      responses:
        200:
          description: OK
    delete:
      summary: Delete a student's record
      parameters:
        - name: id
          type: number
          required: true
          in: query
      responses:
        200:
          description: OK

  /module/:
    get:
      summary: Get a module's details
      parameters:
        - name: code
          type: number
          required: true
          in: query
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/Module'
    post:
      summary: Get a module's details
      parameters:
        - name: code
          type: number
          required: true
          in: query
        - name: title
          type: string
          in: query
        - name: tutor
          type: number
          in: query
      responses:
        200:
          description: OK
    put:
      summary: Create a new module
      parameters:
        - name: code
          type: number
          required: true
          in: query
        - name: title
          type: string
          in: query
          required: true
        - name: tutor
          type: number
          in: query
          required: true
      responses:
        200:
          description: OK
    delete:
      summary: Delete a module
      parameters:
        - name: code
          type: number
          required: true
          in: query
      responses:
        200:
          description: OK

  /coursework/:
    get:
      summary: Get a coursework's details
      parameters:
        - name: number
          type: number
          in: query
          required: true
      responses:
        200:
          description: OK
          schema:
            $ref: '#/definitions/Coursework'

    put:
      summary: Create a new coursework
      parameters:
        - name: number
          type: number
          in: query
          required: true
        - name: title
          type: string
          in: query
          required: true
        - name: due_date
          type: string
          in: query
          required: true
        - name: assessment_type
          type: string
          in: query
          required: true
        - name: module_percentage
          type: number
          format: float
          in: query
          required: true
        - name: tutor_responsible
          type: number
          in: query
          required: true
        - name: module_code
          type: number
          in: query
          required: true
      responses:
        200:
          description: OK

    post:
      summary: Update a new coursework
      parameters:
        - name: number
          type: number
          in: query
          required: true
        - name: title
          type: string
          in: query
        - name: due_date
          type: string
          in: query
        - name: assessment_type
          type: string
          in: query
        - name: module_percentage
          type: number
          format: float
          in: query
        - name: tutor_responsible
          type: number
          in: query
        - name: module_code
          type: number
          in: query
      responses:
        200:
          description: OK

  /work/:
    get:
      summary: Return a list of a student's work, could be simplified e.g. remove date and submitted parameters
      parameters:
        - name: student_id
          type: number
          in: query
          required: true
        - name: module_code
          type: number
          in: query
        - name: issue_date
          type: string
          in: query
        - name: due_date
          type: string
          in: query
        - name: submitted
          type: boolean
          in: query
        - name: assessment_type
          type: string
          in: query
      responses:
        200:
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Coursework'


definitions:
  Student:
    type: object
    properties:
      id:
        type: number
      name:
        type: string
  Staff:
    type: object
    properties:
      id:
        type: number
      name:
        type: string
  Coursework:
    type: object
    properties:
      number:
        type: number
        description: a coursework's unique id
      title:
        type: string
      issue_date:
        type: string # or int if we use unix time
      due_date:
        type: string # see above
      assessment_type:
        type: string
      module_percentage:
        type: number
        format: float
      tutor_responsible:
        type: number
      module_code:
        type: number
  Module:
    type: object
    properties:
      number:
        type: number
      title:
        type: string
      tutor:
        type: number
