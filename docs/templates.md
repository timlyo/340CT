# An Explanation of the Flask/Jinja 2 template rendering system

Feel free to open an issue to discuss any of this.

## Inheritance

[Flask Doc](http://flask.pocoo.org/docs/0.11/patterns/templateinheritance/)

Template Inheritance allows a template to reuse aspects of other webpages without having to duplicate the code on every page. For example the `layout.html` template includes everything that is used on every page, such as libraries, style sheets and the usability assistant. `layout.html` is then extended by every other template to include this.

A Template can also define blocks that allow sections to be easily overridden. `layout.html` defines 4 blocks: head, title, body, and centre.

The head can be overridden to change what is included in the page header. If something is just to be added, e.g. new library, then super can be used within the bock to include everything already in the inherited block. [see here](http://flask.pocoo.org/docs/0.11/patterns/templateinheritance/)

The Body can be overridden to completely change the layout of a page, including removing side margins, or the centre can be overridden to create a centred page with a navigation bar, sharing styles common to all pages. Generally overriding centre is what you'll want. see index for an example.

title can be overridden to change the page title.

A template can also include another. This is basically the same as copy pasting the file in at that position. `layout.html` includes `navigation.html` and `messages.html` in this way.

## Variables

A template can render variables that are passed to it by putting the variables in double curly braces {{*variable*}}. See `student.html` for an example.

A variable is passed to a template in the `render_template` call, see `student_page` function in `routes.py`.

## Loops

A template can render multiple items within a list or dictionary by using a for loop. See `students.html` for an example

## Creating a simple new page

Create a new html file in the templates directory extend `layout.html` to include common functions, and then create a block called centre to start in.

This will include all needed libraries, and create a centred webpage.

In short, copy and paste this into a file

```
{% extends "layout.html" %}

{% block centre %}
	<p>Content goes here</p>
{% endblock %}
```

A route will then need to be added to `routes.py`.
