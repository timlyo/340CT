main: css

css:\
	static/css/style.css

install:
	@mkdir -p /srv/http/340
	@mkdir -p /var/log/340

	# Server
	cp -r server /srv/http/340/
	cp -r static /srv/http/340/
	cp -r templates /srv/http/340/
	cp main.py /srv/http/340/
	cp config/nginx.conf /srv/http/340/
	cp config/uwsgi.ini /srv/http/340/

	# Database
	cp -r database /srv/http/340/
	cp config/database.service /usr/lib/systemd/system/

	cp -r markov /srv/http/340/

	# Filesystem
	chown -R http:log /var/log/340
	chown -R http:http /srv/http/340
	ln -sf /srv/http/340/uwsgi.ini /etc/uwsgi/vassals/340.ini

	sudo systemctl daemon-reload
	systemctl restart emperor.uwsgi
	systemctl restart database

clean:
	rm static/css/style.css
	rm static/css/style.css.map

# Rules

static/css/%.css: sass/%.sass
	@mkdir -p static/css
	sassc $< $@
