import psycopg2
import datetime
from functools import wraps

def connect_and_execute(command: str, params: list):
    with psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432") as conn:
        cursor = conn.cursor()
        cursor.execute(command, params)
        return cursor

def all_students (): #return a list of all students, showing their ID, name and the course they are on
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432") #conn is the database connection
    cur = conn.cursor() #create a cursor to access the database
    cur.execute("SELECT Student_ID, Student_Name, Course_Code FROM Student") #execute an sql statement
    rows = cur.fetchall() #fetch all results from the cursor
    student = [] #start an array
    for row in rows: #populate a dictionary with the results
        dict = {"Student ID" : row[0], "Student Name" : row[1], "Course Code" : row[2]} #name the columns
        student.append(dict) #append the student array with the dictionary
    conn.close() #close the connection
    return (student) #return the array

def all_staff (): #return a list of all staff, showing their ID and Name
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT Staff_ID, Staff_Name FROM Staff")
    rows = cur.fetchall()
    staff = []
    for row in rows:
        dict = {"Staff ID" : row[0], "Staff Name" : row[1]}
        staff.append(dict)
    conn.close()
    return (staff)

def all_modules (): #return a list of all modules
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Module")
    rows = cur.fetchall()
    module = []
    for row in rows:
        dict = {"Module Code" : row[0], "Module ID" : row[1], "Module Title" : row[2]}
        module.append(dict)
    conn.close()
    return (module)

def all_courses (): #return a list of all courses
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Course")
    rows = cur.fetchall()
    course = []
    for row in rows:
        dict = {"Course Code" : row[0], "Course Name" : row[1]}
        course.append(dict)
    conn.close()
    return (course)

def all_courseworks (): #return a list of all courseworks
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Coursework")
    rows = cur.fetchall()
    coursework = []
    for row in rows:
        dict = {"Coursework Number" : row[0], "Coursework Name" : row[1]}
        coursework.append(dict)
    conn.close()
    return (coursework)

def all_course_modules (): #return a list of all modules
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Course_Module")
    rows = cur.fetchall()
    course_module = []
    for row in rows:
        dict = {"Course Code" : row[0], "Module Code" : row[1], "Course and Module Unique ID" : row[2]}
        course_module.append(dict)
    conn.close()
    return (course_module)

def all_submissions  (): #return a list of all submissions
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Submission")
    rows = cur.fetchall()
    submission = []
    for row in rows:
        dict = {"Submission ID" : row[0], "Student ID" : row[1], "Coursework Number" : row[2], "Date Submitted" : row[3], "Mark" : row[4]}
        submission.append(dict)
    conn.close()
    return (submission)

def select_student (Student_ID): #return an individual student
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Student WHERE Student_ID = %s", [Student_ID])
    rows = cur.fetchall()
    student = []
    for row in rows:
        student.append({"id" : row[0], "name" : row[1], "course" : row[2], "password": row[3]})
    conn.close()
    if len(student) == 1:
        return student[0]
    else:
        return None

def select_staff (Staff_ID): #return an individual staff member
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Staff WHERE Staff_ID = %s", [Staff_ID])
    rows = cur.fetchall()
    staff = []
    for row in rows:
        dict = {"id" : row[0], "name" : row[1], "password": row[2]}
        staff.append(dict)
    conn.close()
    if len(staff) == 1:
        return staff[0]
    else:
        return None

def select_coursework (Coursework_Number): #return an individual coursework
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Coursework WHERE Coursework_Number = %s", [Coursework_Number])
    rows = cur.fetchall()
    coursework = []
    for row in rows:
        dict = {"Coursework Number" : row[0], "Coursework Name" : row[1]}
        coursework.append(dict)
    conn.close()
    return (coursework)

def select_module (Module_Code): #return an individual module
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Module WHERE Module_Code = %s", [Module_Code])
    rows = cur.fetchall()
    module = []
    for row in rows:
        dict = {"Module Code" : row[0], "Module Name" : row[1], "Module Tutor" : row[2]}
        module.append(dict)
    conn.close()
    return (module)

def select_course (Course_Code): #return an individual course
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Course WHERE Course_Code = %s", [Course_Code])
    rows = cur.fetchall()
    course = []
    for row in rows:
        dict = {"Course Code" : row[0], "Course Name" : row[1]}
        course.append(dict)
    conn.close()
    return (course)

def select_submission  (Submission_ID): #return an individual submission
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("SELECT * FROM Submission WHERE Submission_ID = %s", [Submission_ID])
    rows = cur.fetchall()
    submission = []
    for row in rows:
        dict = {"Submission ID" : row[0], "Student ID" : row[1], "Coursework Number" : row[2], "Date Submitted" : row[3], "Mark" : row[4]}
        submission.append(dict)
    conn.close()
    if len(submission) == 1:
        return submission[0]
    else:
        return None

def get_users_submissions(student_id) -> map:
    """Get a list of submissions by a single user along with the names of the submissions"""
    query = """
        SELECT Submission.*, Coursework.Coursework_Title
        FROM Submission
        INNER JOIN Coursework
        ON Submission.Coursework_Number=Coursework.Coursework_Number
        WHERE Student_ID = %s"""

    return map (
            lambda row: {
                "submission_id": row[0],
                "student_id": row[1],
                "coursework_number": row[2],
                "date": row[3],
                "mark": row[4],
                "title": row[5]},
            connect_and_execute(query, [student_id]).fetchall()
        )

def add_student (Student_ID, Student_Name, Course_Code, password): #add a student
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("INSERT INTO Student (Student_ID, Student_Name, Course_Code, Student_Password) VALUES (%s, %s, %s, %s)",(Student_ID, Student_Name, Course_Code, password))
    conn.commit() #commit changes
    conn.close()

def remove_student (Student_ID): #remove a student
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("DELETE FROM Student WHERE Student_ID = %s", (Student_ID,))
    conn.commit()
    conn.close()

def add_staff (Staff_ID, Staff_Name, password): # add a member of staff
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("INSERT INTO Staff (Staff_ID, Staff_Name, Staff_Password) VALUES (%s, %s, %s)", (Staff_ID, Staff_Name, password))
    conn.commit()
    conn.close()

def remove_staff (Staff_ID): #remove a member of staff
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("DELETE FROM Staff WHERE Staff_ID = %s", (Staff_ID,))
    conn.commit()
    conn.close()

def add_course (Course_Code, Course_Name): #add a course
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("INSERT INTO Course (Course_Code, Course_Name) VALUES (%s, %s)", (Course_Code, Course_Name))
    conn.commit()
    conn.close()

def remove_course (Course_Code): #remove a course
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("DELETE FROM Course WHERE Course_Code = %s", [Course_Code])
    conn.commit()
    conn.close()

def add_module (Module_Code, Module_Title, Module_Tutor): #add a module
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("INSERT INTO Module (Module_Code, Module_Title, Module_Tutor) VALUES (%s, %s, %s)", (Module_Code, Module_Title, Module_Tutor))
    conn.commit()
    conn.close()

def remove_module (Module_Code): #remove a module
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("DELETE FROM Module WHERE Module_code = %s", [Module_Code])
    conn.commit()
    conn.close()

#def add_coursework (Coursework_Number, Coursework_Title, Issue_Date, Due_Date, Assessment_Type, Module_Percentage, Tutor_Responsible, Module_Code): #add a coursework
#    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
#    cur = conn.cursor()
#    cur.execute("INSERT INTO Coursework (Coursework_Number, Coursework_Title, Issue_Date, Due_Date, Assessment_Type, Module_Percentage, Tutor_Responsible, Module_Code) \
#VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (Coursework_Number, Coursework_Title, Issue_Date, Due_Date, Assessment_Type, Module_Percentage, Tutor_Responsible, Module_Code))
#    conn.commit()
#    conn.close()

def remove_coursework (Coursework_Number): #remove a coursework
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("DELETE FROM Coursework WHERE Coursework_Number = %s", [Coursework_Number])
    conn.commit()
    conn.close()

def add_submission (Student_ID, Coursework_Number): #add a submission
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("INSERT INTO Submission (Student_ID, Coursework_Number, Date_Submitted) VALUES (%s, %s, now())", (Student_ID, Coursework_Number))
    conn.commit()
    conn.close()

def remove_submission (Submission_ID): #remove a submission
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("DELETE FROM Submission WHERE Submission_ID = %s", [Submission_ID])
    conn.commit()
    conn.close()

def update_mark_submission (Mark, Submission_ID): #update the mark for a submission
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("UPDATE Submission SET Mark = %s WHERE Submission_ID = %s", [Mark, Submisison_ID])
    conn.commit()
    conn.close()

def link_course_module (Course_Code, Module_Code, Course_Module_ID): #link a course and module
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("INSERT INTO Course_Module (Course_Code, Module Code, Course_Module_ID) VALUES (%s, %s, %s)", (Course_Code, Module_Code, Course_Module_ID))
    conn.commit()
    conn.close()

def unlink_course_module (Course_Module_ID): #unlink a course and module
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    cur.execute("DELETE FROM Course_Module WHERE Course_Module_ID = %s", [Course_Module_ID])
    conn.commit()
    conn.close()
    
def add_coursework (Coursework_Number, Coursework_Title, Due_Date, Assessment_Type, Module_Percentage, Staff_ID, Module_Code): #sets a new coursework that is assigned to the current staff member
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    
    cur.execute("INSERT INTO Coursework (coursework_number, coursework_title, issue_date, due_date, assessment_type, module_percentage, tutor_responsible, module_code) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (Coursework_Number, Coursework_Title, datetime.datetime.strftime(datetime.datetime.now(), '%Y/%m/%d %H:%M:%S'), Due_Date, Assessment_Type, Module_Percentage, Staff_ID, Module_Code))

def download_coversheets (Student_ID): #return the list of coversheets for self
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()

    cur.execute("SELECT Coursework.Module_Code FROM Coursework WHERE Module_Code = (SELECT Module_Code FROM Course_Module WHERE Course_Code = (SELECT Course_Code FROM Student WHERE Student_ID = %s))", [Student_ID])

    rows = cur.fetchall()
    coversheets = []
    for row in rows:
        dict = {"Module Code" : row[0]}
        coversheets.append(dict)
    conn.close()
    return (coversheets)
    
def select_student_coursework_details (Student_ID): #return the list of courseworks for a given student
    conn = psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432")
    cur = conn.cursor()

    cur.execute("SELECT Coursework.Coursework_Number, Coursework.Coursework_Title, Coursework.Module_Name, Coursework.Module_Code, Submission.Mark, Submission.Date_Submitted FROM Coursework LEFT JOIN Submission ON Coursework.Module_Code = Submission.Module_Code WHERE Module_Code = (SELECT Module_Code FROM Course_Module WHERE Course_Code = (SELECT Course_Code FROM Student WHERE Student_ID = %s ))", [Student_ID])
    rows = cur.fetchall()
    courseworks = []
    for row in rows:
        dict = {"Coursework Number" : row[0],"Coursework Title" : row[1], "Module Name" : row[2], "Module Code" : row[3], "Mark" : row[4], "Date Submitted" : row[5]}
        courseworks.append(dict)
    conn.close()
    return (courseworks)