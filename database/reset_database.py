""" Drop all tables to rerun setup """

import psycopg2

with psycopg2.connect(database="340CT", user="postgres", password="340CT", host="127.0.0.1", port="5432") as conn:
    cur = conn.cursor()
    cur.execute("""
        DROP TABLE IF EXISTS
        Staff, Module, Course, Student, Coursework, Course_Module, Submission
        CASCADE
        """)
