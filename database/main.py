import hug
import functions as database
import psycopg2
import datetime
from falcon import HTTP_409, HTTP_404, HTTP_422, HTTP_500

# Student

@hug.get("/student")
def get_student(response, id=None):
	"""" Get a student's details

	returns a single student if id is passed, or a list of all students if no id is passed

	Responds with a 404 if there is no student with that id

	"""
	if id:
		student = database.select_student(id)
		if student:
			return student
		else:
			response.status = HTTP_404
	else:
		return database.all_students()

@hug.put("/student")
def add_student(id: int, name, course, password, response):
	try:
		database.add_student(id, name, course, password)
	except psycopg2.IntegrityError as e:
		print(e)
		response.status = HTTP_409
		return e.pgerror
	except psycopg2.ProgrammingError as e:
		print(e)
		response.status = HTTP_500
		return e.pgerror


@hug.delete("/student")
def delete_student(id):
	database.remove_student(id)


# Staff

@hug.get("/staff")
def get_students(id=None):
	if id:
		staff = database.select_staff(id)
		print(staff)
		if staff:
			return staff
		else:
			response.status = HTTP_404
	else:
		return database.all_staff()

@hug.put("/staff")
def create_staff(id: int, name: str, password: str, response):
	try:
		database.add_staff(id, name, password)
	except psycopg2.IntegrityError as e:
		print(e)
		response.status = HTTP_409
		return e.pgerror
	except psycopg2.ProgrammingError as e:
		print(e)
		response.status = HTTP_500
		return e.pgerror

# Course

@hug.post("/course")
def add_course(code, name):
	database.add_course(code, name)

# Module

@hug.get("/module")
def get_modules(code=None):
	if code is None:
		return database.all_modules()

@hug.post("/module")
def add_module(code, title, tutor):
	database.add_module(code, title, tutor)

# Submission

@hug.get("/submission")
def get_submission(response, submission_id=None, user_id=None):
	if submission_id: # Return specific submission
		submission = database.select_submission(submission_id)
		print(submission)
		if submission:
			return submission
		else:
			response.status = HTTP_404

	if user_id: # Return all of a user's coursework
		print("getting user submissions")
		submissions = database.get_users_submissions(user_id)
		return submissions

	# Return all if no specific actions are taken
	return database.all_submissions()

@hug.put("/submission")
def add_submission(student_id, coursework_number, response):
	try:
		database.add_submission(student_id, coursework_number)
	except psycopg2.IntegrityError as e:
		response.status = HTTP_422
		return e.pgerror
	except psycopg2.ProgrammingError as e:
		response.status = HTTP_500
		return e.pgerror
		
@hug.put("/add_coursework")
def add_coursework(coursework_number, coursework_title, due_date, assessment_type, module_percentage, tutor_responsible, module_code, response):
	try:
		database.add_coursework(coursework_number, coursework_title, due_date, assessment_type, module_percentage, tutor_responsible, module_code)
	except psycopg2.IntegrityError:
		response.status = HTTP_409
		
# CW_Report

@hug.get("/cw_report")
def cw_report(Coursework_Number):
	report = database.cw_report(Coursework_Number)
	return report

# For cwstatus page	
@hug.get("/cwstatus")
def get_courseworks(student_id):
	if student_id:
		return database.select_student_coursework_details(student_id)