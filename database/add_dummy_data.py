import functions as database
from datetime import datetime
from passlib.hash import sha256_crypt

try:
    database.add_course("CS", "Computer Science")
except Exception as e:
    print(e)

try:
    database.add_student(1, "marmaduke", "CS", sha256_crypt.encrypt("password1"))
except Exception:
    print(e)

try:
    database.add_student(2, "Knob head", "CS", sha256_crypt.encrypt("password2"))
except Exception:
    print(e)

try:
    database.add_staff(1, "admin", sha256_crypt.encrypt("password"))
except Exception as e:
    print(e)

try:
    database.add_module(1, "adafdgsg", 1)
except Exception as e:
    print(e)

try:
    database.add_coursework(1, "Random Crap", datetime.now(), datetime.now(), "boring", 0, 1, 1)
except Exception as e:
    print(e)

try:
    database.add_submission(1, 1)
except Exception as e:
    print(e)
