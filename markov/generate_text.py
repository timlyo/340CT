try:
    from markov import model
except ImportError:
    import model

import random

def generate_text(model):
    text = []
    text.append(random.choice(model[None]))
    while text[-1] is not None:
        text.append(random.choice(model[text[-1]]))

    return text

def generate_string(model):
    text = generate_text(model)
    final = []
    for group in text[:-1]:
        final.append(group[0])
    return " ".join(final)


if __name__ == "__main__":
    model = model.load_model()
    print(generate_string(model))
