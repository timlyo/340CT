import json
import pickle

def load_model():
    model_location = __file__.replace("model.py", "model.pkl")
    with open(model_location, "rb") as file:
        return pickle.load(file)


def save_model(model):
    model_location = __file__.replace("model.py", "model.pkl")
    with open(model_location, "wb") as file:
        return pickle.dump(model, file, pickle.HIGHEST_PROTOCOL)
