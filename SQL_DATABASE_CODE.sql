CREATE Table Staff -- Create Staff table that contains all staff members with their ID
(
	Staff_ID INT NOT NULL PRIMARY KEY, -- primary key to uniquely identify staff
	Staff_Name VARCHAR(30) NOT NULL, -- 30 characters max for their names
	Staff_Password CHAR(77) NOT NULL -- the hashed password for staff members
);

CREATE TABLE Module -- Create Module table containing the details of all the modules
(
	Module_Code VARCHAR(8) NOT NULL PRIMARY KEY, -- 8 characters max to uniquely identify each module
	Module_Title VARCHAR(60) NOT NULL CHECK (Module_Title NOT LIKE '%[^A-Z0-9]%'), -- 60 characters for name of the module, can only have letters, numbers and ';,' characters
	Module_Tutor INT NOT NULL, -- foreign key referencing Staff(Staff_ID)
		CONSTRAINT M_Module_Tutor_Foreign FOREIGN KEY (Module_Tutor) REFERENCES Staff(Staff_ID)
);

CREATE TABLE Course -- Create Course table with Course_Code as the primary key, this contains all courses
(
	Course_Code VARCHAR(8) NOT NULL PRIMARY KEY, -- primary key to uniquely identify all courses
	Course_Name VARCHAR(60) NOT NULL -- 30 characters for name of the course
);

CREATE Table Student -- Create Student table containing all student IDs, names and the course they are on
(
	Student_ID INT NOT NULL PRIMARY KEY, -- Primary key to uniquely identify all students
	Student_Name VARCHAR(30) NOT NULL, -- 30 Character long name
	Course_Code VARCHAR(8) NOT NULL, -- foreign key that references Course(Course_Code) to link the student to the course
	Student_Password CHAR(77) NOT NULL, -- hashed password for students
		CONSTRAINT S_Course_Code_Foreign FOREIGN KEY (Course_Code) REFERENCES Course(Course_Code)
);

CREATE TABLE Coursework -- Create Coursework table, which does not contain submissions, just the coursework framework
(
	Coursework_Number VARCHAR(8) NOT NULL PRIMARY KEY, -- Primary key that identifies the specific coursework
	Coursework_Title VARCHAR(30) NOT NULL, -- Name of the coursework
	Issue_Date DATE NOT NULL, -- when the coursework was issued
	Due_Date DATE NOT NULL, -- when the coursework is due
	Assessment_Type VARCHAR(30) NOT NULL, -- flexible what can be in here, my idea is to put groupwork, individual etc.
	Module_Percentage DECIMAL(5,2) NOT NULL CHECK (Module_Percentage <= 100.00 AND Module_Percentage >= 0), -- percentage of module grade to 2dp, just checks that it is <= 100.00
	Tutor_Responsible INT NOT NULL, -- foreign key that checks the staff member linked to the coursework, Staff(Staff_ID)
	Module_Code VARCHAR(8) NOT NULL, -- foreign key that checks the module associated with the coursework, Module(Module_Code)
		CONSTRAINT CW_Tutor_Responsible_Foreign FOREIGN KEY (Tutor_Responsible) REFERENCES Staff(Staff_ID),
		CONSTRAINT CW_Module_Code_Foreign FOREIGN KEY (Module_Code) REFERENCES Module(Module_Code)
);

CREATE TABLE Course_Module -- This table links the each module and course with a unique ID
(
	Course_Code VARCHAR(8) NOT NULL, -- foreign key that checks Course(Course_Code)
	Module_Code VARCHAR(8) NOT NULL, -- foreign key that checks Module(Module_Code)
	Course_Module_ID SERIAL PRIMARY KEY NOT NULL, -- unique ID that automatically increments by one for each entry
		CONSTRAINT C_M_Course_Code_Foreign FOREIGN KEY (Course_Code) REFERENCES Course(Course_Code),
		CONSTRAINT C_M_Module_Code_Foreign FOREIGN KEY (Module_Code) REFERENCES Module(Module_Code)
);


CREATE TABLE Submission -- Create a submission table to keep track of all submitted
(
	Submission_ID SERIAL NOT NULL PRIMARY KEY, -- unique ID that automatically increments by one for each entry
	Student_ID INT NOT NULL, -- foreign key that checks Student(Student_ID)
	Coursework_Number VARCHAR(8) NOT NULL, -- foreign key that checks Coursework(Coursework_Number)
	Date_Submitted TIMESTAMP NOT NULL, -- the timestamp that the CW was submitted
	Mark DECIMAL(5,2) CHECK (Mark <= 100.00 AND Mark >= 0), -- can be null, as when the student submits they will not instantly have a mark, but cannot be less than 0 or greater than 100
	Status VARCHAR(30), -- can be null, should start as Submitted when the work is originally submitted, Being Graded when marked, and when confirmed, Graded
		CONSTRAINT Su_Student_ID_Foreign FOREIGN KEY (Student_ID) REFERENCES Student(Student_ID),
		CONSTRAINT Su_Coursework_Number_Foreign FOREIGN KEY (Coursework_Number) REFERENCES Coursework(Coursework_Number)
);
