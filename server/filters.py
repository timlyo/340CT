from . import app

from datetime import datetime

@app.template_filter("date_time")
def filter_readable_date_time(value: datetime) -> str:
    if isinstance(value, str):
        value = datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%f")

    if value.year == datetime.now().year:
        return datetime.strftime(value, "%d-%b %H:%M")
    else:
        return datetime.strftime(value, "%d-%b-%y %H:%M")
