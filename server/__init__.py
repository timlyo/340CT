from flask import Flask
import logging

app = Flask(__name__, template_folder="../templates", static_folder="../static")
app.secret_key = "super duper secret" # TODO set by runtime config

@app.before_first_request
def setup_logging():
    if not app.debug:
        # In production mode, add log handler to sys.stderr.
        app.logger.addHandler(logging.StreamHandler())
        app.logger.setLevel(logging.ERROR)

from . import routes, filters
