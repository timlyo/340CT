import os

data_directory = os.path.join(os.getcwd(), "data")

def save_file(file, filename: str) -> None:
    extension = file.filename.split(".")[-1]
    location = os.path.join(data_directory, filename + "." + extension)
    file.save(location)
