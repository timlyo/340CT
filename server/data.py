import requests
from . import data
from passlib.hash import sha256_crypt

def is_user_correct(id: int, password: str, staff=False) -> bool:
    """ Checks if id and password are correct

    id checking works by letting the database throw an exception if the id does not exist
    """
    try:
        if not staff: # Student Logic
            details = data.get("student", id=id)
            return sha256_crypt.verify(password, details["password"])
        else:
            details = data.get("staff", id=id) # Staff Logic
            return sha256_crypt.verify(password, details["password"])

    except KeyError: # User not found
        return False

def put(route: str, **kwargs) -> None:
    if route[0] != "/":
        route = "/{}".format(route)
    print("PUT", route, "with", kwargs)

    url = "http://127.0.0.1:8000{}".format(route)

    result = requests.put(url, data=kwargs)
    print(result.status_code)


    if result.status_code == 409:
        raise ValueError(result.text)
    elif result.status_code == 422:
        raise RuntimeError(result.text)
    elif result.status_code != 200:
        raise Exception("Non 200 status from put to {} \n {} : {}".format(route, result.status_code, result.text))

    result.raise_for_status()


def get(route: str, **kwargs) -> dict:
    """ Make a get request to the database

    Formats the key word arguments into a json structure
    arg1=False, arg2=2 becomes {'arg1'=false,'arg2'=2}
    """

    # Ensure access to route node
    if route[0] != "/":
        route = "/{}".format(route)
    print("GET", route, "with", kwargs)

    # Add path to database uri
    url = "http://127.0.0.1:8000{}".format(route)

    # send request
    result = requests.get(url, params=kwargs)
    print(result.status_code)

    # Error response handling
    if result.status_code == 404: # Not found
        raise KeyError("Not Found")
    elif result.status_code != 200: # Any other error
        raise Exception("Non 200 status from get to {} \n {}".format(result.status_code, result.text))

    # Raise exception if found
    result.raise_for_status()
    # Format the database reponse as json and return it
    return result.json()
