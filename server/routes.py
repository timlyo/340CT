from flask import render_template, request, redirect, flash, session
import requests
import hashlib
from passlib.hash import sha256_crypt

from functools import wraps

from . import app, system, data

import datetime
import markov

model = markov.model.load_model()

def check_staff(route):
    @wraps(route)
    def wrapper():
        if session.get("staff") == True:
            return route()
        flash("Staff only page")
        return redirect("/login")
    return wrapper

# Index route
@app.route("/")
def index():
    # Generate dummy article
    article = markov.generate_text.generate_string(model)
    article = article.replace("\n", "<br>") # For html formatting
    # Return rendered html with article inserted
    return render_template("index.html", article=article)


@app.route("/student")
@app.route("/students")
@check_staff
def student_list():
    students = data.get("student")
    return render_template("students.html", students=students)


@app.route("/staff")
@check_staff
def staff_list():
    staff = data.get("staff")
    return render_template("staff.html", staff=staff)

@app.route("/profile")
def student_page():
    id = session.get("id")
    if session.get("staff"):
        staff = data.get("staff", id=id)
        return render_template("profile.html", staff=staff)
    else:
        student = data.get("student", id=id)
        return render_template("profile.html", student=student)


@app.route("/upload/<coursework_number>", methods=["GET"])
def upload_page(coursework_number):
    user_id = session["id"]
    module = {"id": coursework_number, "name": coursework_number}
    previous_uploads = data.get("submission", user_id=user_id)

    return render_template("upload.html", module=module, previous_uploads=previous_uploads)


@app.route("/upload/<coursework_number>", methods=["POST"])
def recieve_upload(coursework_number):
    user_id = session["id"]
    file = request.files["file"]
    hash = hashlib.sha256(file.read()).hexdigest()
    file.seek(0)

    system.save_file(file, hash)

    try:
        data.put("submission", student_id=user_id, coursework_number=coursework_number)
    except RuntimeError as e:
        return str(e), 422

    return "ok"


@app.route("/login", methods=["GET"])
def login_page():
    return render_template("login.html")


@app.route("/login", methods=["POST"])
def login():
    id = request.form.get("id")
    password = request.form.get("password")
    staff = request.form.get("staff") == "on"

    if data.is_user_correct(id, password, staff=staff):
        route = "staff" if staff else "student"
        user = data.get(route, id=id)

        session["name"] = user["name"]
        session["id"] = id
        session["staff"] = staff

        return redirect("/")
    else:
        flash("Incorrect id or password")
        return render_template("login.html")


@app.route("/logout", methods=["GET", "POST"])
def logout():
    session.clear()
    return redirect("/")

@app.route("/coversheets")
def coversheets():
	student_id = session.get("id")
	sheets = data.get("coversheets", student_id = student_id)
	return render_template("coversheets.html", sheets = sheets)
	
@app.route("/cwstatus")
def cwstatus():
	student_id = session.get("id")
	courseworks = data.get("courseworks", student_id = student_id)
	return render_template("cwstatus.html", courseworks = courseworks)

################
# Admin routes #
################


@app.route("/admin", methods=["GET"])
@check_staff
def admin_page():
    if session.get("id") is None:
        print(session)
        flash("Must be logged in to use admin page")
        return redirect("/login")
    else:
        return render_template("admin.html")


@app.route("/add_user", methods=["POST"])
@check_staff
def add_user():
    name = request.form.get("name")
    student_id = int(request.form.get("id"))
    course = request.form.get("course")
    password = request.form.get("password")

    password = sha256_crypt.encrypt(password)

    try:
        data.put("student", name=name, id=student_id, course=course, password=password)
    except ValueError as e:
        flash(str(e))
    except requests.exceptions.HTTPError as err:
        flash(str(err), "error")
    else:
        flash("Added user {}".format(name), "success")

    return redirect("/admin")


@app.route("/add_staff", methods=["POST"])
@check_staff
def add_staff():
    name = request.form.get("name")
    staff_id = int(request.form.get("id"))
    password = request.form.get("password")

    password = sha256_crypt.encrypt(password)

    try:
        data.put("staff", name=name, id=staff_id, password=password)
    except ValueError:
        flash("User with id {} already exists".format(staff_id), "error")
    except requests.exceptions.HTTPError as err:
        flash(str(err), "error")
    else:
        flash("Added user {}".format(name), "success")

    return redirect("/admin")

@app.route("/help", methods=["GET"])
def help():
    return render_template("help.html") 
 
@app.route("/addcoursework", methods=["GET"])
@check_staff
def addcoursework():
    if session.get("id") is None:
        print(session)
        flash("Must be logged in to use admin page")
        return redirect("/login")
    else:
        return render_template("addcoursework.html")

@app.route("/add_coursework", methods=["POST"])
def add_coursework():
    coursework_number = int(request.form.get("coursework_number"))
    coursework_title = request.form.get("coursework_title")
    due_date = request.form.get("due_date")
    assessment_type = request.form.get("assessment_type")
    
    tutor_responsible = session.get("id")
    module_code = request.form.get("module_code")
    
    if int(request.form.get("module_percentage")) < 0:
        flash("Module percentage cannot be less than 0", "error")
    elif int(request.form.get("module_percentage")) > 100:
        flash("Module percentage cannot be greater than 100", "error")
    elif int(request.form.get("module_percentage")) >= 0 and int(request.form.get("module_percentage")) <= 100:
        module_percentage = int(request.form.get("module_percentage"))
        try:
            data.put("add_coursework", coursework_number=coursework_number, coursework_title=coursework_title, due_date=due_date, assessment_type=assessment_type, module_percentage=module_percentage, tutor_responsible=tutor_responsible, module_code=module_code)
        except ValueError:
            flash("Coursework {} already exists".format(coursework_number), "error")
        except requests.exceptions.HTTPError as err:
            flash(str(err), "error")
        else:
            flash("Added coursework {}".format(coursework_title), "success")
    
    return redirect("/addcoursework")


#the below route checks if the user is logged in as an admin or not, forwarding them to the login page if they aren't
#and sending them to the cw_report.html page if they are
@app.route("/cwreport", methods=["GET"]) 
@check_staff
def cwreport():
    if session.get("id") is None:
        print(session)
        flash("Must be logged in to use admin page")
        return redirect("/login")
    else:
        return render_template("cw_report.html")
        
#this route obtains the Coursework_Number variable from the form, passes that value to the cw_report function, stores the results in the report variable
#and rerenders the template with the new information
@app.route("/cw_report", methods=["GET", "POST"])
def cw_report():
	Coursework_Number = request.form.get("Coursework_Number")
	report = data.get("cw_report", Coursework_Number=Coursework_Number)
	print (report)
	return render_template("cw_report.html", Coursework_Number=Coursework_Number, report=report)
